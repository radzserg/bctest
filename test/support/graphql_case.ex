defmodule Bct.GraphqlCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use ExUnit.Case

      def evaluate_graphql(query, options \\ []) do
        Absinthe.run(query, Bct.Graphql.Schema, options)
      end

      def assert_graphql(query, expectation, options \\ []) do
        result = evaluate_graphql(query, options)

        assert result == {:ok, %{data: expectation}}
      end
    end
  end
end
