defmodule Bct.Test.Checksum.GraphqlTest do
  use Bct.GraphqlCase, async: true

  import Mox

  setup :verify_on_exit!

  test "it gets checksum" do
    query = """
      {
        checksum
      }
    """

    Bct.Checksum.TestStorage |> expect(:get, fn -> [5, 4, 8, 9, 8, 5, 0, 3, 5, 4] end)

    assert {:ok, %{data: %{"checksum" => 7}}} == evaluate_graphql(query)
  end

  test "it adds numbers" do
    mutation = """
      mutation checksumAdd($number: Int!) {
        result: checksumAdd(number: $number)
      }
    """

    Bct.Checksum.TestStorage |> expect(:put, fn 12 -> :ok end)

    assert {:ok, %{data: %{"result" => true}}} ==
             evaluate_graphql(mutation, variables: %{"number" => 12})
  end

  test "it clears numbers" do
    mutation = """
      mutation checksumClear {
        result: checksumClear
      }
    """

    Bct.Checksum.TestStorage |> expect(:clear, fn -> :ok end)
    assert {:ok, %{data: %{"result" => true}}} == evaluate_graphql(mutation)
  end
end
