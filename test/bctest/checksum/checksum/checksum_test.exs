defmodule Bct.Test.Checksum.ChecksumTest do
  use ExUnit.Case, async: true

  alias Bct.Checksum.Checksum

  test "it calculates checksum correctly" do
    digits = [5, 4, 8, 9, 8, 5, 0, 3, 5, 4]
    assert Checksum.checksum(digits) == 7
  end

  test "it calculates checksum correctly for empty list" do
    digits = []

    assert Checksum.checksum(digits) == 0
  end

  test "it calculates checksum correctly when rem is lower than 10" do
    digits = [1, 2, 1]

    assert Checksum.checksum(digits) == 2
  end
end
