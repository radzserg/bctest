defmodule Bct.Test.Checksum.AgentStorageTest do
  use ExUnit.Case, async: true

  alias Bct.Checksum.AgentStorage

  test "it adds and stores numbers" do
    AgentStorage.clear()
    assert AgentStorage.get() == []

    AgentStorage.put(12)
    assert AgentStorage.get() == [1, 2]

    AgentStorage.put(1)
    assert AgentStorage.get() == [1, 2, 1]

    AgentStorage.put(22)
    assert AgentStorage.get() == [1, 2, 1, 2, 2]
  end

  test "it clears bucket" do
    AgentStorage.clear()
    assert AgentStorage.get() == []

    AgentStorage.put(12)
    AgentStorage.clear()
    assert AgentStorage.get() == []
  end

  test "only numbers can be added" do
    assert_raise RuntimeError, "Only numbers can be added", fn ->
      AgentStorage.put("12")
    end
  end
end
