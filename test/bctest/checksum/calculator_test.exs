defmodule Bct.Test.Checksum.CalculatorTest do
  use ExUnit.Case, async: true

  import Mox

  setup :verify_on_exit!

  test "put new numbers to storage" do
    Bct.Checksum.TestStorage
    |> expect(:put, fn 22 -> :ok end)
    |> expect(:put, fn 33 -> :ok end)

    assert Bct.Checksum.Calculator.add_number(22) == :ok
    assert Bct.Checksum.Calculator.add_number(33) == :ok
  end

  test "clear storage" do
    Bct.Checksum.TestStorage
    |> expect(:clear, fn -> :ok end)

    assert Bct.Checksum.Calculator.clear() == :ok
  end

  test "it calculates checksum correctly" do
    Bct.Checksum.TestStorage |> expect(:get, fn -> [5, 4, 8, 9, 8, 5, 0, 3, 5, 4] end)

    assert Bct.Checksum.Calculator.checksum() == 7
  end
end
