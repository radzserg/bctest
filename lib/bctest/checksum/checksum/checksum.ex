defmodule Bct.Checksum.Checksum do
  require Integer

  @doc """
  Calculates checksum

  1. Add up the digits in odd positions and multiply by 3
  2. Add up the digits in even positions
  3. Add up the results of 1 and 2
  4. Divide by 10 and take the remainder.
  5. If the remainder is 0 final result is 0 otherwise subtract it from 10 for final result

  Example:

  5 4 8 9 8 5 0 3 5 4

  1. 5 + 8 + 8 + 0 + 5 = 26 * 3 = 78
  2. 4 + 9 + 5 + 3 + 4 = 25
  3. 78 + 25 = 103
  4. 103 / 10 = 10.3 = 3
  5. 10 - 3 = 7
  """
  def checksum(digits) do
    {evens_sum, ods_sum} = evens_ods_sum(digits)
    sum = evens_sum + ods_sum * 3
    rem = rem(sum, 10)
    if rem == 0, do: 0, else: 10 - rem
  end

  defp evens_ods_sum(digits) do
    digits
    |> Enum.with_index()
    |> Enum.reduce({0, 0}, fn {digit, index}, {evens_sum, odds_sum} ->
      if Integer.is_even(index) do
        {evens_sum, digit + odds_sum}
      else
        {digit + evens_sum, odds_sum}
      end
    end)
  end
end
