defmodule Bct.Checksum.Storage do
  @doc """
  Returns all saved digits as a list of digits
  """
  @callback get() :: [number]

  @doc """
  Saves new number to the storage
  """
  @callback put(number :: number) :: :ok

  @doc """
  Clear previously received list of digits
  """
  @callback clear() :: :ok
end
