defmodule Bct.Checksum.AgentStorage do
  @moduledoc """
  Simple storage that keeps state inside the agent.
  """

  use Agent

  @behaviour Bct.Checksum.Storage

  @doc """
  Starts a new bucket.
  """
  def start_link(_opts) do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  @doc """
  Gets a value from the `bucket` by `key`.
  """
  @spec get() :: [number]
  def get() do
    Agent.get(__MODULE__, fn state -> state end)
  end

  @doc """
  Adds number to the bucket
  """
  @spec put(number :: number) :: :ok
  def put(number) when is_number(number) do
    Agent.update(__MODULE__, fn state ->
      Integer.digits(number)
      |> Enum.reduce(state, fn digit, state ->
        state ++ [digit]
      end)
    end)
  end

  def put(_number) do
    raise "Only numbers can be added"
  end

  @doc """
  Reset bucket
  """
  @spec clear() :: :ok
  def clear() do
    Agent.update(__MODULE__, fn _state -> [] end)
  end
end
