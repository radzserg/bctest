defmodule Bct.Checksum.Graphql do
  use Absinthe.Schema.Notation

  object :checksum_queries do
    query do
      @desc "Get checksum"
      field :checksum, :integer do
        resolve(&checksum/2)
      end
    end
  end

  object :checksum_mutations do
    @desc "add number"
    field :checksum_add, :boolean do
      arg(:number, non_null(:integer))
      resolve(&add_numbers/2)
    end

    @desc "clear saved numbers"
    field :checksum_clear, :boolean do
      resolve(&clear_numbers/2)
    end
  end

  @doc """
  Returns checksum of stored numbers
  """
  def checksum(_args, _resolution) do
    checksum = Bct.Checksum.Calculator.checksum()
    {:ok, checksum}
  end

  @doc """
  Adds numbers to storage
  """
  def add_numbers(%{number: number}, _resolution) do
    :ok = Bct.Checksum.Calculator.add_number(number)
    {:ok, true}
  end

  @doc """
  Clear stored numbers
  """
  def clear_numbers(_args, _resolution) do
    :ok = Bct.Checksum.Calculator.clear()
    {:ok, true}
  end
end
