defmodule Bct.Checksum.Calculator do
  @storage Application.get_env(:bctest, :storage)

  @max_lifetime 15

  @doc """
  Adds new numbers to
  """
  @spec add_number(number :: number) :: :ok
  def add_number(number) do
    @storage.put(number)
  end

  @doc """
  Clears previously saved digits
  """
  @spec clear() :: :ok
  def clear() do
    @storage.clear()
  end

  @doc """
  Calculates checksum for the saved digits list
  """
  @spec checksum() :: :ok
  def checksum() do
    digits = @storage.get()

    try do
      checksum_task =
        Task.async(fn ->
          Bct.Checksum.Checksum.checksum(digits)
        end)

      Task.await(checksum_task, @max_lifetime)
    catch
      :exit, _ -> {:error, nil}
    end
  end
end
