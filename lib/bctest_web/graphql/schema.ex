defmodule Bct.Graphql.Schema do
  use Absinthe.Schema

  import_types(Bct.Graphql.ContentTypes)

  import_types(Bct.Checksum.Graphql)

  query do
    import_fields(:checksum_queries)
  end

  mutation do
    import_fields(:checksum_mutations)
  end
end
