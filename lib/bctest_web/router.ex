defmodule BctestWeb.Router do
  use BctestWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api" do
    pipe_through :api

    forward "/graphiql", Absinthe.Plug.GraphiQL, schema: Bct.Graphql.Schema

    forward "/", Absinthe.Plug, schema: Bct.Graphql.Schema
  end
end
