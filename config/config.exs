# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :bctest, BctestWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aiiElb97icg0qlY+GIJw0vJ0vNnBym7hOAiJWEJh11aMUqM4p1u0dSx2oVTsASul",
  render_errors: [view: BctestWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Bctest.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :bctest, :storage, Bct.Checksum.AgentStorage

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
